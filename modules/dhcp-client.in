#!/bin/bash

# Copyright (c) 2016, Alfredo Mungo
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from this
# software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# This module configures and runs the DHCP client (dhclient).
#
# REQUIRED GLOBALS
#  PROFILE_IFACE: Interface to run the DHCP client for
#  DHCP_CONFIG: DHCP client config file path
#  DHCP_PIDFILE: PID file for DHCP client
#
# PROVIDED routines
#  dhcp_client_run()
#  dhcp_client_status()
#  dhcp_client_stop()

DHCP_CONFIG="@@ETCDIR@@/dhclient.conf"
DHCP_PIDFILE="@@RUNDIR@@/$PROFILEHASH-dhcp.pid"

# Runs DHCP client
# Usage: dhcp_client_run
dhcp_client_run() {
  dhclient -pf $DHCP_PIDFILE -cf $DHCP_CONFIG -nw $PROFILE_IFACE
}

# Reports status about DHCP client
# Usage: dhcp_client_status
dhcp_client_status() {
  [[ -f $DHCP_PIDFILE ]]
}

# Stops DHCP client
# Usage: dhcp_client_stop
dhcp_client_stop() {
  [[ -f $DHCP_PIDFILE ]] && dhclient -pf $DHCP_PIDFILE -x $PROFILE_IFACE
}
