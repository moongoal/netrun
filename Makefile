SHELL=/bin/bash
DESTDIR=
MANSEC=1
PREFIX=/usr/local
export ETCDIR=/etc
export BINDIR=$(PREFIX)/bin
export LIBDIR=$(PREFIX)/lib
export RUNDIR=/var/run
export DATAROOTDIR=$(PREFIX)/share

export VERSION_MAJOR=1
export VERSION_MINOR=0
export VERSION_PATCH=0
export PROFILE_OWNER=root
export PROFILE_GROUP=root
export PROFILEDIR_PERMS=rwxr-xr-x

export MANDIR=$(DATAROOTDIR)/man/man$(MANSEC)
export CFGDIR=$(ETCDIR)/netrun
export SHRDIR=$(DATAROOTDIR)/netrun
export XMPDIR=$(SHRDIR)/example
export MODDIR=$(LIBDIR)/netrun/modules

export HOOKDIR=$(CFGDIR)/hooks
export HKSDIR=$(HOOKDIR)/start
export HKFDIR=$(HOOKDIR)/finish

EXAMPLES=wpa2-psk
MODULES=dhcp-client wpa-supplicant

EXAMPLES:=$(addprefix examples/,$(EXAMPLES))
MODULES:=$(addprefix modules/,$(MODULES))

.PHONY: install doc modules

all: netrun doc modules

netrun $(MODULES): %: %.in
		./subs.sh < $< > $@

install: all
	install -d -m755 $(DESTDIR){$(CFGDIR),$(MANDIR),$(SHRDIR),$(XMPDIR),$(BINDIR),$(MODDIR),$(HKSDIR),$(HKFDIR)}
	install -m755 netrun $(DESTDIR)$(BINDIR)/
	install -m644 $(EXAMPLES) $(DESTDIR)$(XMPDIR)/
	install -m644 netrun.$(MANSEC) $(DESTDIR)$(MANDIR)/
	install -m755 $(MODULES) $(DESTDIR)$(MODDIR)/

doc: netrun.$(MANSEC)

modules: $(MODULES)

netrun.$(MANSEC): netrun.pod
		./subs.sh < $< | \
		pod2man -u --center="Netrun network profile manager" --date="October 2016" --name="netrun" --section=$(MANSEC) --release="v$(VERSION_MAJOR).$(VERSION_MINOR).$(VERSION_PATCH)" > $@

README.md: netrun.pod
	./subs.sh < $< | \
	pod2markdown -u > $@

clean:
	$(RM) netrun{,.1} $(MODULES)
