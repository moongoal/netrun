# NAME

netrun - Network profile manager

# SYNOPSIS

netrun \[OPTIONS\] **{PROFILE}**

# OPTIONS

- -s|--status

    Query profile status

- -t|--stop

    Stop profile if running

- -h|--help

    Print help message and exit

# DESCRIPTION

Netrun runs the profile script associated with the given profile, found in
`/etc/netrun` after checking its permission flags. For a given PROFILE, the
`/etc/netrun/PROFILE` script is run. Profile files must be executable by the
current user in order for them to be enabled. To disable a profile, switch its
executable flag off. The default umask for running profiles is 640.

## Profile files

Each profile file is a shell script containing at least the following routine
declarations:

- profile\_run()

    This routine is called whenever the profile is run (unless profile\_status()
    returns 0) and shall set the profile up and running. Its return value will be
    the netrun exit value.

- profile\_status()

    This routine is called when the -s|--status option is passed and shell return 1
    if the profile is not running, 0 if is running.

- profile\_stop()

    This routine is called when the -t|--stop option is passed and shell disable any
    service started by profile\_run(), bring down any interfaces previously brought
    up and clean temporary files produced during profile\_run().

And the following variable declarations:

- PROFILE\_TYPE

    Defines which type of profile the current file represents. Currently only _wireless_,_ethernet_,_ppp_ are supported.

- PROFILE\_IFACE

    Defines the target interface. In case of multiple interfaces a number can be appended to the name, such as `PROFILE_IFACE1`, `PROFILE_IFACE2`, ...

## Environment

The following routines are available to each profile:

- fatal(msg)

    Prints an error message to stderr and exit with error.

- error(msg)

    Prints an error message to stderr and return 1.

The following global variables are available to each profile:

- PROFILEDIR

    Profile files directory.

- MODDIR

    Modules directory.

- PROFILE\_OWNER, PROFILE\_GROUP

    Profile files owner and group.

- COMMAND

    Command begin executed. Must be one of _run_, _status_, _stop_.

- PROFILE

    Current profile name.

- PROFILE\_PATH

    Current profile file path.

- PROFILEHASH

    Current profile path hash (SHA256); might come in handy to generate profile-specific configuration files/paths.

## Modules

To reduce the complexity of the profiles, modules are provided. A module is inserted into the current profile by calling `module MODNAME` and configured through global variables. Some modules define default values for their configuration. To override default values, declare the global variable after inserting the module; to prefer the default value (if any), declare the global variable before inserting the module. Each module is documented inside its own file in `/usr/local/lib/netrun/modules`.

Currently the following modules are provided by this package:

- wpa-supplicant
- dhcp-client

## Hooks
To perform actions before/after running a profile, hooks are provided. Netrun places its start hooks in `/etc/netrun/hooks/start` and finish hooks in `/etc/netrun/hooks/finish`. Hooks are simply run inside the netrun shell by inclusion with `source`. Hooks are provided the very same variables available to profiles (and modifying them in start hooks will make the changes available to profile scripts). They are also provided a `HOOKSTATE` variable which can be _start_ or _finish_ based on how the hook file is run (this way you are able to deploy both start and finish hooks in a single file, creating a link). Start hooks are run immediately before any profile action is executed and finish hooks are executed immediately after any profile action completes. Finish hooks are not run if the profile exits before completing.

# EXIT STATUS

On success 0 is returned, on failure a non-zero value is returned.

# FILES AND DIRECTORIES

- `/etc/netrun`

    Configuration directory containing the profile files

- `/usr/local/lib/netrun/modules`

    Module directory containing the available module files

- `/usr/local/share/netrun/example`

    Example profile files directory

- `/etc/netrun/hooks/start` and `/etc/netrun/hooks/finish`

    Start and finish hooks directories

# AUTHOR

Written by Alfredo Mungo <alfredo.mungo@openmailbox.org>.

# COPYRIGHT

Copyright © 2016 Alfredo Mungo. License BSD 3-Clause
[https://opensource.org/licenses/BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause).
