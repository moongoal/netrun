#!/bin/bash

# Perform substitution on STDIN and output to STDOUT

perl -pe "s/\@\@VERSION_MAJOR\@\@/${VERSION_MAJOR}/g;" \
	-e "s/\@\@VERSION_MINOR\@\@/${VERSION_MINOR}/g;" \
	-e "s/\@\@VERSION_PATCH\@\@/${VERSION_PATCH}/g;" \
	-e "s#\@\@ETCDIR\@\@#${ETCDIR}#;" \
	-e "s#\@\@CFGDIR\@\@#${CFGDIR}#;" \
	-e "s#\@\@MANDIR\@\@#${MANDIR}#;" \
	-e "s#\@\@SHRDIR\@\@#${SHRDIR}#;" \
	-e "s#\@\@XMPDIR\@\@#${XMPDIR}#;" \
	-e "s#\@\@BINDIR\@\@#${BINDIR}#;" \
	-e "s#\@\@MODDIR\@\@#${MODDIR}#;" \
	-e "s#\@\@RUNDIR\@\@#${RUNDIR}#;" \
	-e "s#\@\@HKSDIR\@\@#${HKSDIR}#;" \
	-e "s#\@\@HKFDIR\@\@#${HKFDIR}#;" \
	-e "s/\@\@PROFILE_OWNER\@\@/${PROFILE_OWNER}/g;" \
	-e "s/\@\@PROFILE_GROUP\@\@/${PROFILE_GROUP}/g;" \
	-e "s/\@\@PROFILEDIR_PERMS\@\@/${PROFILEDIR_PERMS}/g;"
