=encoding utf8

=head1 NAME

netrun - Network profile manager


=head1 SYNOPSIS

netrun [OPTIONS] B<{PROFILE}>


=head1 OPTIONS

=over

=item -s|--status

Query profile status

=item -t|--stop

Stop profile if running

=item -h|--help

Print help message and exit

=back


=head1 DESCRIPTION

Netrun runs the profile script associated with the given profile, found in
F<@@CFGDIR@@> after checking its permission flags. For a given PROFILE, the
F<@@CFGDIR@@/PROFILE> script is run. Profile files must be executable by the
current user in order for them to be enabled. To disable a profile, switch its
executable flag off. The default umask for running profiles is 640.

=head2 Profile files

Each profile file is a shell script containing at least the following routine
declarations:

=over

=item profile_run()

This routine is called whenever the profile is run (unless profile_status()
returns 0) and shall set the profile up and running. Its return value will be
the netrun exit value.

=item profile_status()

This routine is called when the -s|--status option is passed and shell return 1
if the profile is not running, 0 if is running.

=item profile_stop()

This routine is called when the -t|--stop option is passed and shell disable any
service started by profile_run(), bring down any interfaces previously brought
up and clean temporary files produced during profile_run().

=back

And the following variable declarations:

=over

=item PROFILE_TYPE

Defines which type of profile the current file represents. Currently only I<wireless>,I<ethernet>,I<ppp> are supported.

=item PROFILE_IFACE

Defines the target interface. In case of multiple interfaces a number can be appended to the name, such as C<PROFILE_IFACE1>, C<PROFILE_IFACE2>, ...

=back


=head2 Environment

The following routines are available to each profile:

=over

=item fatal(msg)

Prints an error message to stderr and exit with error.

=item error(msg)

Prints an error message to stderr and return 1.

=back

The following global variables are available to each profile:

=over

=item PROFILEDIR

Profile files directory.

=item MODDIR

Modules directory.

=item PROFILE_OWNER, PROFILE_GROUP

Profile files owner and group.

=item COMMAND

Command begin executed. Must be one of I<run>, I<status>, I<stop>.

=item PROFILE

Current profile name.

=item PROFILE_PATH

Current profile file path.

=item PROFILEHASH

Current profile path hash (SHA256); might come in handy to generate profile-specific configuration files/paths.

=back


=head2 Modules

To reduce the complexity of the profiles, modules are provided. A module is inserted into the current profile by calling C<module MODNAME> and configured through global variables. Some modules define default values for their configuration. To override default values, declare the global variable after inserting the module; to prefer the default value (if any), declare the global variable before inserting the module. Each module is documented inside its own file in F<@@MODDIR@@>.

Currently the following modules are provided by this package:

=over

=item wpa-supplicant

=item dhcp-client

=back


=head2 Hooks
To perform actions before/after running a profile, hooks are provided. Netrun places its start hooks in F<@@HKSDIR@@> and finish hooks in F<@@HKFDIR@@>. Hooks are simply run inside the netrun shell by inclusion with C<source>. Hooks are provided the very same variables available to profiles (and modifying them in start hooks will make the changes available to profile scripts). They are also provided a C<HOOKSTATE> variable which can be I<start> or I<finish> based on how the hook file is run (this way you are able to deploy both start and finish hooks in a single file, creating a link). Start hooks are run immediately before any profile action is executed and finish hooks are executed immediately after any profile action completes. Finish hooks are not run if the profile exits before completing.


=head1 EXIT STATUS

On success 0 is returned, on failure a non-zero value is returned.


=head1 FILES AND DIRECTORIES

=over

=item F<@@CFGDIR@@>

Configuration directory containing the profile files

=item F<@@MODDIR@@>

Module directory containing the available module files

=item F<@@XMPDIR@@>

Example profile files directory

=item F<@@HKSDIR@@> and F<@@HKFDIR@@>

Start and finish hooks directories

=back


=head1 AUTHOR

Written by Alfredo Mungo E<lt>alfredo.mungo@openmailbox.orgE<gt>.


=head1 COPYRIGHT

Copyright © 2016 Alfredo Mungo. License BSD 3-Clause
L<https://opensource.org/licenses/BSD-3-Clause>.
